<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('douban.php');

$douban_t=isset($_SESSION['douban_t'])?$_SESSION['douban_t']:'';

//检查是否已登录
if($douban_t!=''){
	$douban=new doubanPHP($douban_k, $douban_s, $douban_t);

	//获取登录用户信息
	$result=$douban->me();
	var_dump($result);

	/**
	//access token到期后使用refresh token刷新access token
	$result=$douban->access_token_refresh($callback_url, $_SESSION['douban_r']);
	var_dump($result);
	**/

	/**
	//发布分享
	$text='分享内容';
	$title='分享标题';
	$url='http://www.oschina.net/';
	$result=$douban->share($text, $title, $url);
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：获取登录用户信息
	$result=$douban->api('v2/user/~me', array(), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$douban=new doubanPHP($douban_k, $douban_s);
	$login_url=$douban->login_url($callback_url, $scope);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
